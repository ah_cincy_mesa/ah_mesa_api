var express = require('express');
var path = require('path');
var router = express.Router();
var mongoose = require('mongoose');
var restaurents = require('../models/restaurents');
var reservations = require('../models/reservations');
var uuid = require('uuid');
var randomstring = require("randomstring");

/*GET urls*/
router.get('/', function(req, res){
	res.writeHead(200, {'Content-Type': 'text/plain'});
  	res.end('https://bitbucket.org/ah_cincy_mesa/ah_mesa_api/wiki/Home');
	});

/*GET heartbeat*/
router.get('/heartbeat', function(req, res){
	res.writeHead(200, {'Content-Type': 'text/plain'});
  	res.end('i\'m still alive');
	});

/*GET restaurents*/
router.get('/restaurants/:restaurantId', function(req,res){
	restaurents.find({id:req.params.restaurantId}, function(err, restaurants) {
	  if (err) return console.error(err);
	  	res.setHeader('Content-Type', 'application/json');
	 	res.json(restaurants);
	});
});

/*GET restaurents*/
router.get('/restaurants', function(req,res){
	restaurents.find(function(err, restaurants) {
	  if (err) return console.error(err);
	  	res.setHeader('Content-Type', 'application/json');
	 	res.json(restaurants);
	});
});


/*GET reservations for a restaurent*/
router.get('/reservations/:restaurentId', function(req,res){
 	reservations.find({restaurantId:req.params.restaurentId}, function(err, reservations){
    if (err) return console.error(err);
    res.setHeader('Content-Type', 'application/json');
    res.json(reservations);
	});
});

/*POST update restaurant waitTime*/
router.post('/restaurants/:restaurantId/waitTime', function(req,res){
	var waitTime = req.body.waitTime;
	restaurents.findOneAndUpdate({id: req.params.restaurantId},  {waitTime: waitTime}, function(err){
        if(err)
            res.json(err);
        else    
            res.json();
    });
});

/*GET reservations for a user*/
router.get('/user/:userId', function(req,res){
 	reservations.find({userId:req.params.userId}, function(err, reservations){
    if (err) return console.error(err);
    res.setHeader('Content-Type', 'application/json');
    res.json(reservations.filter(function(x){return x.closed==false}));
	});
});

/*POST reservations*/
router.post('/reservations', function(req,res){
   var tStamp = Date.now();
   var reserv = new reservations({
   								  reservationId: randomstring.generate(7),
   								  userId: req.body.userId, 
                                  customerName: req.body.customerName, 
                                  restaurantId: req.body.restaurantId, 
                                  partySize: req.body.partySize,
                                  waitTime: req.body.waitTime,
                                  tStamp:tStamp.toString(),
                                  notes: req.body.notes,
                              	  closed: false,
                              	  seatingNow: false,
                              	  responded: false});
   console.log(reserv)
   res.setHeader('Content-Type', 'application/json');
   reserv.save(function(err){
     if(err)
		res.json(err);
     else
        res.json();
      });
});

/*POST reservations notify*/
router.post('/reservations/:reservationId/notify', function(req,res){
    reservations.findOneAndUpdate({reservationId: req.params.reservationId},  {seatingNow: true}, function(err){
        if(err)
            res.json(err);
        else    
            res.json();
    });
});

/*POST reservations close*/
router.post('/reservations/:reservationId/close', function(req,res){
    reservations.findOneAndUpdate({reservationId: req.params.reservationId},  {closed: true}, function(err){
        if(err)
            res.json(err);
        else    
            res.json();
    });
});

/*POST reservations close*/
router.post('/reservations/:reservationId/accept', function(req,res){
    reservations.findOneAndUpdate({reservationId: req.params.reservationId},  {responded: true}, function(err){
        if(err)
            res.json(err);
        else    
            res.json();
    });
});


/*POST reservations seated*/
router.post('/user/:userId/seated', function(req,res){
    reservations.update({userId: req.params.userId}, {closed: true}, {multi: true}, function(err, numberAffected, rawResponse) {
        if(err)
            res.json(err);
        else    
            res.json();
})
});

module.exports = router;