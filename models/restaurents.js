var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var restaurentsSchema = mongoose.Schema({ 
 	name: String,
    id: String,
  	description: String,
  	mobileCheckIn: Boolean,
  	waitTime:String,
  	location: String,
    priceRange: String,
    rating: String,
  	address: {line1: String,
  			  city: String,
  			  state: String,
  			  zipcode: String}
}); 
var restaurents = mongoose.model('restaurents', restaurentsSchema);
module.exports = restaurents;