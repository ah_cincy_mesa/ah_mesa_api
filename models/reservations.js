var mongoose = require('mongoose');
var Schema = mongoose.Schema;

 var reservationSchema = mongoose.Schema({ 
 	reservationId: String,
    restaurantId: String,
  	userId: String,
  	partySize: String,
  	responded: Boolean,
  	seatingNow: Boolean,
  	closed: Boolean,
  	customerName: String,
  	tStamp: String,
  	waitTime: String,
  	notes: String
}); 
var reservations = mongoose.model('reservations', reservationSchema);
module.exports = reservations;